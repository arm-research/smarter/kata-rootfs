# kata-rootfs

Script to build an initrd based rootfs to use with kata-containers.

Use this script on an arm64 machine to build a rootfs.


Prerequisites:

  * golang
  * docker
  * sudo (required to remove the roofs directory)

Set the GOPATH environment variable

Usage:

`./build.sh PATH_TO_GO_BIN_DIR DISTRO`


Tested values for distro are: "alpine"

Example invocation:

`GOPATH=/home/username ./build.sh /usr/local/go/bin alpine`







