#!/bin/bash -fvx

distro=$1

GOPATH=`pwd`

echo "Getting source from github.com/kata-containers/osbuilder"
go get -d -u github.com/kata-containers/osbuilder

export ROOTFS_DIR=${GOPATH}/src/github.com/kata-containers/osbuilder/rootfs-builder/rootfs

sd=`pwd`

cd $GOPATH/src/github.com/kata-containers/osbuilder/rootfs-builder

echo "Building rootfs."
echo GOPATH=$GOPATH AGENT_INIT=yes USE_DOCKER=true SECCOMP=no  ./rootfs.sh ${distro}
GOPATH=$GOPATH AGENT_INIT=yes SECCOMP=no ./rootfs.sh ${distro}

echo "Building initrd image"
AGENT_INIT=yes ${GOPATH}/src/github.com/kata-containers/osbuilder/initrd-builder/initrd_builder.sh -o ${sd}/kata-containers-initrd.img  ${ROOTFS_DIR}



